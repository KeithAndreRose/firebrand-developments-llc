import React from "react";
import "../styles/components/ServicesContainer.scss";

export default function ServicesContainer() {
  return (
    <div className={"services-container"}>
      <ul className="services-list">
        <li className="services-item">Web Design and Developement</li>
        <li className="services-item">Web Consulting</li>
        <li className="services-item">Mentoring</li>
      </ul>
      <span className="services-disclaimer">* Contact for details</span>
    </div>
  );
}
