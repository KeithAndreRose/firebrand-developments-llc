import React from "react";
import "../styles/components/ContentArticle.scss";

export default function ContentArticle({ headerContent,label, contents }) {
  return (
    <article className="content-article">
      <div className="content-article-header">
        {headerContent}
      </div>
      <div className="content-article-body">
        <h3 className="content-article-body-label">{label}</h3>
        <p className="content-article-body-paragraph">{contents}</p>
      </div>
    </article>
  );
}
