import React from "react";
import "../styles/components/ProjectBadge.scss";
import { GamedevDictionaryLogo } from "./svgs";
import SimplyTaskLogo from "./icons/SimplyTaskLogo";

export default function ProjectBadge({ project }) {
  switch (project) {
    case "gamedevDictionary": {
      return (
        <div className="ProjectContainer ProjectContainer-gamedevDictionary">
          <a
            id="GamedevDictionary"
            className="ProjectBadge ProjectBadge-gamedevDictionary"
            href="https://gamedevdictionary.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <GamedevDictionaryLogo />
          </a>
          <label className="ProjectContainer-label">Gamedev Dictionary</label>
        </div>
      );
    }
    case "simplyTask": {
      return (
        <div className="ProjectContainer ProjectContainer-simplyTask">
          <a
            id="SimplyTask"
            className="ProjectBadge ProjectBadge-simplyTask"
            href="https://simplytask.app"
            target="_blank"
            rel="noopener noreferrer"
          >
            <SimplyTaskLogo />
          </a>
          <label className="ProjectContainer-label">Simply Task</label>
        </div>
      );
    }
    default: {
      return (
        <div className="ProjectContainer">
          <div id="ComingSoon" className="ProjectBadge">
            Coming Soon...
          </div>
        </div>
      );
    }
  }
}
