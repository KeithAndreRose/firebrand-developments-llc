import React, { useState } from "react";
import "../styles/components/ContentDrawer.scss";
export default function ContentDrawer({ label, labelIcon, contents, className }) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={`ContentDrawer ${isOpen ? "open" : ""} ${className || ''}`}>
      <span className="content-drawer-label" onClick={() => setIsOpen(!isOpen)}>
        {labelIcon ? (
          <span className="content-drawer-icon">{labelIcon}</span>
        ) : (
          <span className="content-drawer-cursor">{">"}</span>
        )}
        <span>{label}</span>
      </span>
      <div className={`content-drawer`}>{contents}</div>
    </div>
  );
}
