import React from 'react'
import '../../styles/layout/SiteFooter.scss'
import TwitterLogo from '../svgs/TwitterLogo'
import InstagramLogo from '../svgs/InstagramLogo'
export default function SiteFooter() {
  return (
    <footer className="site-footer">
      <div className="site-footer-inner">Firebrand <span role="img" aria-label="flame emoji">🔥</span>&trade;</div>
      <div className="site-footer-extra">
        <span>Founded by Keith Rose</span>
        <span> &copy; 2020 Firebrand Developments LLC. All Rights Reserved.</span>
        <span> Unauthorized duplication, in whole or in part, is strictly prohibited.</span>
        <div className="extra-links">
          <a href="https://twitter.com/firebrandDev">Twitter <TwitterLogo/></a>
          <a href="https://instagram.com/firebrand.dev">Instagram <InstagramLogo/></a>
        </div>
      </div>
      <div className="site-footer-background"></div>
    </footer>
  )
}
