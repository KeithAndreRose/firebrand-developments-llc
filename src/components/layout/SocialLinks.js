import React from "react";
import TwitterLogo from "../svgs/TwitterLogo";
import InstagramLogo from "../svgs/InstagramLogo";
import GithubLogo from "../svgs/GithubLogo";
// import YouTubeLogo from "../svgs/YouTubeLogo";
import "../../styles/layout/SocialLinks.scss";

export default function SocialLinks() {
  return (
    <div className="social-links">
      <p className="social-links-caption">Follow</p>
      <div className="social-links-container">
        <a className="social-link" href="https://twitter.com/firebrandDev">
          <TwitterLogo className="social-link-icon twitter-logo" />
        </a>
        <a className="social-link" href="https://instagram.com/firebrand.dev">
          <InstagramLogo className="social-link-icon instagram-logo" />
        </a>
        <a className="social-link" href="https://github.com/firebrandDev">
          <GithubLogo className="social-link-icon github-logo" />
        </a>
        {/* <a className="social-link" href="https://youtube.com/">
          <YouTubeLogo className="social-link-icon youtube-logo" />
        </a> */}
      </div>
    </div>
  );
}
