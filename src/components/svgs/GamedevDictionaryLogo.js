import React, { useEffect, useRef } from "react";
import '../../styles/icons/GamedevDictionaryLogo.scss'
import { GhostIcon, CodeIcon, DictionaryIcon } from ".";

export default function GamedevDictionaryLogo({ className }) {
  const brandRef = useRef(null)
  useEffect(()=>{
    setTimeout(() => {
      if(brandRef.current) brandRef.current.classList.add("show");
    }, 150);
    setTimeout(() => {
      if(brandRef.current) brandRef.current.classList.remove("show");
    }, 2200);
  },[])
  return (
    <div ref={brandRef} className={`GamedevDictionaryLogo ${className || ""}`}>
      <GhostIcon />
      <CodeIcon />
      <DictionaryIcon />
    </div>
  );
}
