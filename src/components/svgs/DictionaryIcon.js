import React from "react";

export default function DictionaryIcon({ className }) {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 140 193"
    >
      <path d="M5 14l5 0 0 75 10 -14 10 14 0 -75 92 0 3 -2 -119 0c0,0 -1,1 -1,2zm123 170l0 -171 -4 4 0 171 4 -4zm3 -173l0 0 0 170 1 -1 0 -170 -1 1zm-3 -2l2 -2 -121 0c0,1 -1,1 -1,2l120 0zm7 -2l0 170 2 -3 0 -169 -2 2zm-2 -3l1 -1c-39,0 -79,0 -118,0 -1,1 -2,1 -3,1l120 0zm-133 189c0,-56 0,-123 0,-179 3,-7 7,-12 14,-14l126 0 0 176 -16 17 -124 0z" />
    </svg>
  );
}
