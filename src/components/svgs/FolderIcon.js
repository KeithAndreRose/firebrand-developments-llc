import React from "react";

export default function FolderIcon({ className }) {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 32 32"
    >
      <path
        d="M 9,5 A 1.0000999,1.0000999 0 0 0 8,6 v 16 a 1.0000999,1.0000999 0 0 0 1,1 h 18 a 1.0000999,1.0000999 0 0 0 1,-1 V 10 A 1.0000999,1.0000999 0 0 0 27,9 H 17.8867 L 15.8652,5.5 A 1.0000999,1.0000999 0 0 0 15,5 Z"
      ></path>
      <path
        d="M 5,9 C 4.4477,9 4,9.44772 4,10 v 16 c 10e-5,0.55226 0.4477,0.99994 1,1 h 18 a 1.0000001,1.0000001 0 0 0 1,-1 1.0000001,1.0000001 0 0 0 -1,-1 H 6 V 10 C 6,9.44772 5.5523,9 5,9 Z"
      ></path>
    </svg>
  );
}
