import React, { useEffect, useState, useRef } from "react";
import "../styles/components/ContactForm.scss";

export default function ContactForm() {
  const [formData, setFormData] = useState({});
  const [sentContact, setSentContact] = useState(null);
  const contactAreaRef = useRef(null);

  const handleFormSubmit = async (evt) => {
    evt.preventDefault();
    const { current } = contactAreaRef;
    const { name, email, requestedService, message } = formData;

    const data = `
      Name: ${name}
      Email: ${email}
      Requested Service: ${requestedService}
      Message: ${message}
    `;

    current.classList.add("submitting");

    const response = async () =>
      await fetch(
        "https://xmwu5ltlfc.execute-api.us-east-1.amazonaws.com/dev/contact",
        {
          method: "POST",
          mode: "cors",
          credentials: "omit",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            subject: "Firebrand First Contact",
            message: data,
          }),
        }
      );

    try {
      await response();
    } catch (err) {
      current.classList.remove("submitting");
      throw new Error("Error Sending Email");
    }

    current.classList.remove("submitting");

    current.classList.add("submitted");
    localStorage.setItem("sentContact", true);
    setSentContact(true);
    console.log(formData, response);
  };

  const handleInputChange = (evt) => {
    const { target } = evt;
    const { value, name } = target;
    setFormData((data) => {
      return { ...data, [name]: value };
    });
  };

  useEffect(() => {
    setSentContact(localStorage.getItem("sentContact"));
  }, [sentContact]);
  return (
    <section className="ContactForm">
      <div
        className={`contact-area ${sentContact ? "submitted" : ""}`}
        ref={contactAreaRef}
      >
        <form className="contact-form" onSubmit={handleFormSubmit}>
          <input
            name="name"
            type="text"
            placeholder="Name"
            onChange={handleInputChange}
            required
          />
          <input
            name="email"
            type="text"
            placeholder="Contact Email Address"
            onChange={handleInputChange}
            required
          />
          <select name="requestedService" onChange={handleInputChange} required>
            <option value="" defaultValue hidden>
              Service
            </option>
            <option value="Web Design and Development">
              Web Design and Development
            </option>
            <option value="Web Solution">Web Solution</option>
            <option value="Mentoring">Mentoring</option>
          </select>
          <textarea
            name="message"
            placeholder="Describe more about what is needed"
            onChange={handleInputChange}
            required
          />
          <input className="contact-submit" type="submit" value="Submit Contact" />
        </form>
        <div className="contact-submitted">Expect our contact soon</div>
      </div>
    </section>
  );
}
