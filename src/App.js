import React from "react";
import "./App.scss";
import { FirebrandSymbolLogo } from "./components/svgs";
import SocialLinks from "./components/layout/SocialLinks";
import ContentDrawer from "./components/ContentDrawer";
import ContactForm from "./components/ContactForm";
import FolderIcon from "./components/svgs/FolderIcon";
import ServicesIcon from "./components/svgs/ServicesIcon";
import ContactEmailIcon from "./components/svgs/ContactEmailIcon";
import ProjectBadge from "./components/ProjectBadge";
import ServicesContainer from "./components/ServicesContainer";
import SiteFooter from "./components/layout/SiteFooter";

function App() {
  return (
    <>
      <FirebrandSymbolLogo className="brandLogo" />
      <main>
        <section className="landing">
          <ContentDrawer
            className={'showcase-drawer'}
            label="Portfolio/Showcase"
            labelIcon={<FolderIcon />}
            contents={
              <>
                <ProjectBadge project="gamedevDictionary" />
                <ProjectBadge project="simplyTask" />
                <ProjectBadge />
              </>
            }
          />
          <ContentDrawer
            className={'services-drawer'}
            label="Offered Services"
            labelIcon={<ServicesIcon />}
            contents={<ServicesContainer />}
          />
          <ContentDrawer
            className={'contact-drawer'}
            label="Make Contact"
            labelIcon={<ContactEmailIcon />}
            contents={<ContactForm />}
          />
        </section>
        <hr></hr>
        <SocialLinks />
        <SiteFooter />
      </main>
    </>
  );
}

export default App;
